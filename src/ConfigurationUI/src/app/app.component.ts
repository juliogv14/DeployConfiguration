import { Component, OnInit } from '@angular/core';
import { ConfigTab } from 'src/app/Models/ConfigTab';
import { TargetsService } from './targets.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogNewConfigComponent } from './dialog-config-tab/dialog-new-config.component';
import { Setting } from 'src/app/Models/Setting';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'ConfigurationUI';
  tabs: ConfigTab[] = []
  currentTab: ConfigTab

  constructor(protected targetsService: TargetsService, protected dialog: MatDialog) { }

  ngOnInit(): void {
    this.targetsService.getTargets().subscribe(data => {
      this.tabs = data.sort((a, b) => a.Target.localeCompare(b.Target));;
      if (data.length > 0) {
        this.currentTab = this.tabs[0]
      }
    });
  }

  setCurrentTab(tab: string) {
    this.currentTab = this.tabs.find(t => t.Target == tab)
  }

  newConfig() {
    let newConfigName: string;
    const dialogRef = this.dialog.open(DialogNewConfigComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        newConfigName = result;
        let settings: Setting[] = [];
        let newConfig = new ConfigTab(newConfigName, settings);
        this.tabs.push(newConfig);
        this.currentTab = newConfig;

      }
    });
  }
  removeTab(index: number) {
    let target = this.tabs[index].Target;
    if(confirm("Are you sure to delete the configuration page " + target)) {
      this.tabs.splice(index, 1)
      console.log("Remove target " + target)
      this.targetsService.deleteTarget(target)
        .subscribe(
          success => {
            console.log(`Removed target ${target}`)
          },
          error => {
            console.log(`Error saving settings ${error}"`)
          });
      let lastTabIndex : number
      if(index > 0){
        lastTabIndex = index -1;
      }
      this.currentTab = this.tabs[lastTabIndex]
    }

  }
}
