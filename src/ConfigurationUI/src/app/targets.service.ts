import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConfigTab } from 'src/app/Models/ConfigTab';
import { map } from 'rxjs/operators';
import { AppConfigService } from './app-config.service';

@Injectable({
  providedIn: 'root'
})
export class TargetsService {

  private settingsEndpoint: string = "api/settings"

  constructor(protected config : AppConfigService, protected http: HttpClient) { }

  getTargets(): Observable<ConfigTab[]> {
    let url = `${this.config.apiBaseUrl}/${this.settingsEndpoint}/targets`;
    return this.http.get<ConfigTab[]>(url).pipe(
      map((data: any[]) => data.map((item: any) => {
        return new ConfigTab(item, []);
      }))
    );
  }

  deleteTarget(target: string) {
    let url = `${this.config.apiBaseUrl}/${this.settingsEndpoint}/${target}`;
    return this.http.delete(url);
  }
}
