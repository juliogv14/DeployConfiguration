export class SettingModel {
    constructor(
      public Key: string,
      public Target: string,
      public Value: string) { }
  }