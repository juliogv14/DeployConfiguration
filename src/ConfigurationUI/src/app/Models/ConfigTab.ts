import { Setting } from './Setting';

export class ConfigTab {
    constructor(
      public Target: string,
      public Settings: Setting[]) { }
  }