import { Component, OnInit, Input, ElementRef, ViewChild, Output, EventEmitter } from '@angular/core';
import { fromEvent } from 'rxjs';
import { filter, take } from 'rxjs/operators';

@Component({
  selector: 'app-editable-input',
  templateUrl: './editable-input.component.html',
  styleUrls: ['./editable-input.component.scss']
})
export class EditableInputComponent implements OnInit {
  @Input() Text: string;
  @Input() PlaceHolder: string;
  @ViewChild('input') input: ElementRef;
  @Output() update = new EventEmitter();

  mode = Mode
  fieldMode: Mode = Mode.View
  constructor(private host: ElementRef) {

  }
  private get element() {
    return this.host.nativeElement;
  }

  ngOnInit(): void {
    this.viewModeHandler();
    this.editModeHandler();
    if(!this.Text){
      this.fieldMode = Mode.Edit;
    }
  }

  private viewModeHandler() {
    fromEvent(this.element, 'click').subscribe(() => {
      if (this.fieldMode === Mode.View)
        this.fieldMode = Mode.Edit;
    });
  }

  private editModeHandler() {
    fromEvent(document, 'click')
      .pipe(filter(({ target }) => this.element.contains(target) === false))
      .subscribe(() => {
        if (this.fieldMode == Mode.Edit) {
          if (this.Text) {
            this.update.next();
            this.fieldMode = Mode.View;
          }
        }
      })

  }
}

export enum Mode {
  View,
  Edit
}
