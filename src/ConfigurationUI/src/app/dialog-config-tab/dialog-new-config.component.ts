import { Component, Inject } from '@angular/core';
import { MatDialogActions, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'dialog-new-config',
  templateUrl: './dialog-new-config.component.html',
  styleUrls: ['./dialog-new-config.component.scss']
})
export class DialogNewConfigComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogNewConfigComponent>,
    @Inject(MAT_DIALOG_DATA) public name: string) { }

  onCancel(): void {
    this.dialogRef.close();
  }
}