import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {

  private appConfig: any;

  constructor(private http: HttpClient) {  }

  loadAppConfig() {
    return this.http.get('/assets/config.json')
      .toPromise()
      .then(data => {
        this.appConfig = data;
      });
  }

  get apiBaseUrl() {

    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }
    let baseUrl = this.appConfig.apiBaseUrl
    if(baseUrl.includes("localhost")){
      baseUrl = baseUrl.replace("localhost", window.location.hostname)
    }
    return baseUrl;
  }
}