import { Component, OnInit, Input } from '@angular/core';
import { Setting } from 'src/app/Models/Setting';
import { SettingsService } from './settings.service';
import { ConfigTab } from 'src/app/Models/ConfigTab';
import { TargetBinder, ResourceLoader } from '@angular/compiler';
import { Observable, timer } from 'rxjs';

@Component({
  selector: 'app-config-tab',
  templateUrl: './config-tab.component.html',
  styleUrls: ['./config-tab.component.scss']
})
export class ConfigTabComponent implements OnInit {
  private tab: ConfigTab

  @Input()
  set Tab(val: ConfigTab) {
    this.tab = val;
    this.reloadSettings()
  }
  get Tab(): ConfigTab {
    return this.tab;
  }

  public loading: boolean;
  constructor(protected settingsService: SettingsService) { }

  ngOnInit(): void {

  }

  headers = ["Key", "Value"]

  reloadSettings() {
    this.loading = true;
    this.settingsService.getSettings(this.Tab.Target).subscribe(data => {
      this.Tab.Settings = data;
      this.loading = false;
    });
  }

  saveConfig() {
    this.Tab.Settings.forEach(s => console.log(this.Tab.Target, s))
    this.settingsService.putSettings(this.Tab.Target, this.Tab.Settings)
      .subscribe(
        success => {
          alert("Settings saved")
        },
        error => {
          alert(`Error saving settings ${error}"`)
          console.log(error)
        });
  }

  newSetting() {
    let lastSetting = this.Tab.Settings[this.Tab.Settings.length - 1];
    if (!this.Tab.Settings.length || lastSetting.Key) {
      this.Tab.Settings.push(new Setting("", ""));
    }
  }
  updateSetting(setting: Setting, field: string, value: string){
    setting[field]=value;
  }
  removeSetting(index: number){
    this.Tab.Settings.splice(index, 1)
  }
}
