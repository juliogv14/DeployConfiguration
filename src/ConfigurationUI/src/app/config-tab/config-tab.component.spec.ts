import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigTabComponent } from './config-tab.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { table } from 'console';
import { ConfigTab } from '../Models/ConfigTab';
import { Setting } from '../Models/Setting';
import { BrowserModule } from '@angular/platform-browser';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SettingsService } from './settings.service';
import { Settings } from 'http2';
import { Observable } from 'rxjs';
import { of } from 'rxjs';
describe('ConfigTabComponent', () => {
  let component: ConfigTabComponent;
  let mockSettingService : SettingsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigTabComponent ],
      providers: [ { provide: SettingsService, useValue: mockSettingService } ],
      imports: [
        BrowserModule,
        MatIconModule,
        HttpClientTestingModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatDialogModule,
        MatInputModule,
        FormsModule,
        BrowserAnimationsModule, ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    mockSettingService = new SettingsService(null, null)
    component = new ConfigTabComponent(mockSettingService)
    spyOn(mockSettingService, 'getSettings').and.returnValue(of(new Array<Setting>()))
    let settings: Setting[] = [new Setting("key", "val")]
    component.Tab = new ConfigTab("target", settings)

  });
  afterEach(() => {

  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create setting', () => {
    let settings: Setting[] = [new Setting("key", "val")]
    component.Tab = new ConfigTab("target", settings)
    console.log(component.Tab)
    let settingsCountBefore = component.Tab.Settings.length;
    component.newSetting()
    let settingsCountAfter = component.Tab.Settings.length;
    expect(settingsCountAfter).toBeGreaterThan(settingsCountBefore)
  });
});
