import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Setting } from 'src/app/Models/Setting';
import { Observable } from 'rxjs';
import { map, delay } from 'rxjs/operators';
import { SettingModel } from 'src/app/Models/SettingModel';
import { AppConfigService } from '../app-config.service';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  private settingsEndpoint: string = "api/settings"
  constructor(protected config : AppConfigService, protected http: HttpClient) { }

  getSettings(target: string): Observable<Setting[]> {
    let url = `${this.config.apiBaseUrl}/${this.settingsEndpoint}/${target}`;
    return this.http.get<Setting[]>(url).pipe(
      map((data: any[]) => data.map((item: any) => {
        return new Setting(item.key, item.value);
      }))
    );
  }

  putSettings(target: string, settings: Setting[]) {
    let settingModels: SettingModel[] = settings.map(s =>  new SettingModel(s.Key, target, s.Value));
    let url = `${this.config.apiBaseUrl}/${this.settingsEndpoint}/${target}`;
    let httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    return this.http.put<SettingModel[]>(url, settingModels, httpOptions)
  }
}
