﻿using ConfigurationDb.Domain;
using FluentAssertions;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace ConfigurationDb.Tests
{
    [TestFixture]
    public class TestSettingsService
    {
        private readonly string TestingDatabase = "TestSettings";

        
        [TearDown]
        public void TearDown()
        {
            TestsHelper.TearDownSettingsDatabase(TestingDatabase);
        }

        [Test]
        public void Upsert_OK()
        {
            ISettingsRepository repository = new EfSettingsRepository(TestsHelper.GetTestSettingsDbContext(TestingDatabase));
            string target = FillInitialData(repository);

            //Set settings for target
            repository = new EfSettingsRepository(TestsHelper.GetTestSettingsDbContext(TestingDatabase));
            SettingsService settingsService = new SettingsService(repository);
            List<Setting> targetSettings = settingsService.GetSettingsByTarget(target) as List<Setting>;
            var updateSetting = targetSettings.First();
            updateSetting.Value = "NewVal";
            var newSetting = TestsHelper.CreateSetting("newSet", target, "newVal");
            targetSettings.Append(newSetting);
            repository = new EfSettingsRepository(TestsHelper.GetTestSettingsDbContext(TestingDatabase));
            settingsService = new SettingsService(repository);
            settingsService.SetSettingsForTarget(target, targetSettings);

            //Assert 
            repository = new EfSettingsRepository(TestsHelper.GetTestSettingsDbContext(TestingDatabase));
            settingsService = new SettingsService(repository);
            List<Setting> resultSettings = settingsService.GetSettingsByTarget(target).ToList();
            resultSettings.Should().BeEquivalentTo(targetSettings);

            //Delete database
            TestsHelper.TearDownSettingsDatabase(TestingDatabase);
        }

        [Test]
        public void Delete_OK()
        {
            //Arrange
            ISettingsRepository repository = new EfSettingsRepository(TestsHelper.GetTestSettingsDbContext(TestingDatabase));
            string target = FillInitialData(repository);
            repository = new EfSettingsRepository(TestsHelper.GetTestSettingsDbContext(TestingDatabase));
            SettingsService settingsService = new SettingsService(repository);

            //Act
            settingsService.DeleteTarget(target);
            

            //Assert
            repository = new EfSettingsRepository(TestsHelper.GetTestSettingsDbContext(TestingDatabase));
            settingsService = new SettingsService(repository);
            var targets = settingsService.GetTargets();
            Assert.That(!targets.Contains(target));
        }

        public string FillInitialData(ISettingsRepository repository)
        {
            
            //Test data
            string initTarget = "initTarget";
            Setting setting1Target1 = TestsHelper.CreateSetting("set1", initTarget, "val1");
            Setting setting2Target1 = TestsHelper.CreateSetting("set2", initTarget, "val2");

            List<Setting> settingsTarget = new List<Setting>
            {
                setting1Target1, setting2Target1
            };

            ISettingsRepository settingsRepository = repository;

            //Add initTarget
            settingsTarget.ForEach(s => settingsRepository.Add(s));
            settingsRepository.SaveChanges();

            //Get
            IEnumerable<Setting> result = settingsRepository.GetAllByTarget(initTarget);
            Assert.That(result.Count, Is.EqualTo(2));
            result.Should().BeEquivalentTo(settingsTarget);
            return initTarget;
        }
    }
}
