using ConfigurationDb.Domain;
using NUnit.Framework;
using System.Collections.Generic;
using FluentAssertions;
using System.Linq;

namespace ConfigurationDb.Tests
{
    public class Tests
    {
        private readonly string TestingDatabase = "TestSettings";

        

        [Test]
        public void Add()
        {
            SettingsDbContext dbContext = TestsHelper.GetTestSettingsDbContext(TestingDatabase);
            ISettingsRepository settingsRepository = new EfSettingsRepository(dbContext);

            FillInitialData(settingsRepository);

            //Add target2
            string target2 = "target2";
            Setting setting1Target2 = TestsHelper.CreateSetting("set1", target2, "val1");
            Setting setting2Target2 = TestsHelper.CreateSetting("set2", target2, "val2");
            Setting setting3Target2 = TestsHelper.CreateSetting("set3", target2, "val3");

            List<Setting> settingsTarget2 = new List<Setting>
            {
                setting1Target2, setting2Target2, setting3Target2
            };
            settingsTarget2.ForEach(s => settingsRepository.Add(s));
            settingsRepository.SaveChanges();

            //Get
            IEnumerable<Setting> result = settingsRepository.GetAllByTarget(target2);
            Assert.That(result.Count, Is.EqualTo(3));
            result.Should().BeEquivalentTo(settingsTarget2);

            //Delete database
            TestsHelper.TearDownSettingsDatabase(TestingDatabase);
            settingsRepository.Dispose();
        }
        [Test]
        public void Update()
        {
            SettingsDbContext dbContext = TestsHelper.GetTestSettingsDbContext(TestingDatabase);
            ISettingsRepository settingsRepository = new EfSettingsRepository(dbContext);

            string target = FillInitialData(settingsRepository);

            //Update
            Setting set2 = settingsRepository.Find(s => s.Key == "set2" && s.Target == target).FirstOrDefault();
            Assert.IsNotNull(set2);
            set2.Value = "ChangedVal";
            settingsRepository.SaveChanges();

            //Get
            Setting resultSetting = settingsRepository.Find(s => s.Key == "set2" && s.Target == target).FirstOrDefault();
            Assert.IsNotNull(resultSetting);
            Assert.That(resultSetting.Value, Is.EqualTo("ChangedVal"));

            //Delete database
            TestsHelper.TearDownSettingsDatabase(TestingDatabase);
            settingsRepository.Dispose();
        }

        [Test]
        public void Delete()
        {
            SettingsDbContext dbContext = TestsHelper.GetTestSettingsDbContext(TestingDatabase);
            ISettingsRepository settingsRepository = new EfSettingsRepository(dbContext);

            string target = FillInitialData(settingsRepository);

            //Get element to remove
            Setting resultSetting = settingsRepository.Find(s => s.Key == "set1" && s.Target == target).FirstOrDefault();
            Assert.IsNotNull(resultSetting);
            List<Setting> settingsToRemove = new List<Setting>
            {
                resultSetting
            };

            //Delete
            List<Setting> settingsAfterRemove = settingsRepository.GetAllByTarget(target).Except(settingsToRemove).ToList();

            settingsRepository.Delete(settingsToRemove);
            settingsRepository.SaveChanges();

            //Get
            IEnumerable<Setting> result = settingsRepository.GetAllByTarget(target);
            result.Should().BeEquivalentTo(settingsAfterRemove);

            //Delete database
            TestsHelper.TearDownSettingsDatabase(TestingDatabase);
            settingsRepository.Dispose();
        }

        public string FillInitialData(ISettingsRepository settingsRepository)
        {
            //Test data
            string initTarget = "initTarget";
            Setting setting1Target1 = TestsHelper.CreateSetting("set1", initTarget, "val1");
            Setting setting2Target1 = TestsHelper.CreateSetting("set2", initTarget, "val2");

            List<Setting> settingsTarget1 = new List<Setting>
            {
                setting1Target1, setting2Target1
            };

            //Add initTarget
            settingsTarget1.ForEach(s => settingsRepository.Add(s));
            settingsRepository.SaveChanges();

            //Get
            IEnumerable<Setting> result = settingsRepository.GetAllByTarget(initTarget);
            Assert.That(result.Count, Is.EqualTo(2));
            result.Should().BeEquivalentTo(settingsTarget1);
            return initTarget;
        }
    }
}