﻿using ConfigurationDb.Domain;
using Microsoft.EntityFrameworkCore;

namespace ConfigurationDb.Tests
{
    public class TestsHelper
    {
        public static SettingsDbContext GetTestSettingsDbContext(string databaseName)
        {
            var optionsBuilder = new DbContextOptionsBuilder<SettingsDbContext>();
            optionsBuilder.UseInMemoryDatabase(databaseName);
            return new SettingsDbContext(optionsBuilder.Options);
        }

        public static Setting CreateSetting(string key, string target, string value)
        {
            return new Setting
            {
                Key = key,
                Target = target,
                Value = value
            };
        }

        public static void TearDownSettingsDatabase(string database)
        {
            SettingsDbContext dbContext = TestsHelper.GetTestSettingsDbContext(database);
            dbContext.Database.EnsureDeleted();
        }
    }
}
