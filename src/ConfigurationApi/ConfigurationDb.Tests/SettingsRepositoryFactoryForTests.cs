﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConfigurationDb.Tests
{
    public class SettingsRepositoryFactoryForTests
    {
        public ISettingsRepository Create()
        {
            return new EfSettingsRepository(TestsHelper.GetTestSettingsDbContext("TestSettings"));
        }
    }
}
