﻿using AutoMapper;
using ConfigurationApi.Models;
using ConfigurationDb.Domain;
using System.Diagnostics.CodeAnalysis;

namespace ConfigurationApi.Mapper
{
    [ExcludeFromCodeCoverage]
    public class SettingsMapping : Profile
    {
        public SettingsMapping()
        {
            CreateMap<Setting, SettingModel>().ReverseMap();
        }
    }
}
