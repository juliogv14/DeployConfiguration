﻿using AutoMapper;
using ConfigurationApi.Models;
using ConfigurationDb;
using ConfigurationDb.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace ConfigurationApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SettingsController : ControllerBase
    {
        
        private readonly ILogger<SettingsController> logger;
        private readonly ISettingsService settingsService;
        private readonly IMapper mapper;

        public SettingsController(ILogger<SettingsController> logger, ISettingsService settingsService, IMapper mapper)
        {
            this.logger = logger;
            this.settingsService = settingsService;
            this.mapper = mapper;
        }

        [HttpGet("targets")]
        public IActionResult GetTargets()
        {
            var targets = settingsService.GetTargets();
            return Ok(targets);
        }

        [HttpGet("{target}")]
        public IActionResult Get([FromRoute] string target)
        {
            var settings = settingsService.GetSettingsByTarget(target);
            if (!settings.Any())
            {
                return NotFound(target);
            }
            return Ok(settings);
        }

        [HttpDelete("{target}")]
        public IActionResult DeleteTarget([FromRoute] string target)
        {
            if (!this.ModelState.IsValid || string.IsNullOrWhiteSpace(target))
            {
                BadRequest();
            }
            settingsService.DeleteTarget(target);
            return Ok();
        }

        [HttpPut("{target}")]
        public IActionResult Upsert([FromRoute] string target, [FromBody]IEnumerable<SettingModel> settingsModel)
        {
            if(!this.ModelState.IsValid || string.IsNullOrWhiteSpace(target))
            {
                BadRequest();
            }
            IEnumerable<Setting> settings = this.mapper.Map<IEnumerable<SettingModel>, IEnumerable<Setting>>(settingsModel);
            settingsService.SetSettingsForTarget(target, settings);
            return Ok();
        }
    }
}
