﻿using System.ComponentModel.DataAnnotations;

namespace ConfigurationApi.Models
{
    public class SettingModel
    {
        [Required]
        public string Key { get; set; }

        [Required]
        public string Target { get; set; }

        public string Value { get; set; }
    }
}
