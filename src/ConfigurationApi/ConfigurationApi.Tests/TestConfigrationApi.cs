using AutoMapper;
using ConfigurationApi.Controllers;
using ConfigurationApi.Models;
using ConfigurationDb;
using ConfigurationDb.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;

namespace ConfigurationApi.Tests
{
    public class TestConfigrationApi
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Get_Ok()
        {
            var logger = Substitute.For<ILogger<SettingsController>>();
            var service = Substitute.For<ISettingsService>();
            var mapper = Substitute.For <IMapper>();
            var settingsController = new SettingsController(logger, service, mapper);
            string target = "target";
            var settings = new List<Setting>()
            {
                CreateSetting("key", target, "value")
            };
            service.GetSettingsByTarget(target).Returns(settings);
            IActionResult response = settingsController.Get("target");
            Assert.That(response, Is.AssignableFrom<OkObjectResult>());
        }

        [Test]
        public void GetTargets_Ok()
        {
            var logger = Substitute.For<ILogger<SettingsController>>();
            var service = Substitute.For<ISettingsService>();
            var mapper = Substitute.For<IMapper>();
            var settingsController = new SettingsController(logger, service, mapper);
            string target = "target";
            var settings = new List<string>()
            {
                "target1",
                "target2"
            };
            service.GetTargets().Returns(settings);
            IActionResult response = settingsController.GetTargets();
            Assert.That(response, Is.AssignableFrom<OkObjectResult>());
        }

        [Test]
        public void Get_KO()
        {
            //Arrange
            var logger = Substitute.For<ILogger<SettingsController>>();
            var service = Substitute.For<ISettingsService>();
            var mapper = Substitute.For<IMapper>();
            var settingsController = new SettingsController(logger, service, mapper);
            string target = "target";
            service.GetSettingsByTarget(target).Returns(new List<Setting>());
            
            //Act
            IActionResult response = settingsController.Get(target);

            //Assert
            Assert.That(response, Is.AssignableFrom<NotFoundObjectResult>());
        }

        [Test]
        public void Upsert_OK()
        {
            var logger = Substitute.For<ILogger<SettingsController>>();
            var service = Substitute.For<ISettingsService>();
            var mapper = Substitute.For<IMapper>();
            var settingsController = new SettingsController(logger, service, mapper);
            string target = "target";
            var settings = new List<SettingModel>();
            IActionResult response = settingsController.Upsert(target, settings);
            Assert.That(response, Is.AssignableFrom<OkResult>());
        }

        [Test]
        public void Delete_Ok()
        {
            var logger = Substitute.For<ILogger<SettingsController>>();
            var service = Substitute.For<ISettingsService>();
            var mapper = Substitute.For<IMapper>();
            var settingsController = new SettingsController(logger, service, mapper);
            string target = "target";
            service.DeleteTarget(target);
            IActionResult response = settingsController.DeleteTarget(target);
            Assert.That(response, Is.AssignableFrom<OkResult>());
        }

        public static Setting CreateSetting(string key, string target, string value)
        {
            return new Setting
            {
                Key = key,
                Target = target,
                Value = value
            };
        }
    }
}