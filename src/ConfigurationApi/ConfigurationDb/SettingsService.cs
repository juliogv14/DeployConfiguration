﻿using ConfigurationDb.Domain;
using System.Collections.Generic;
using System.Linq;

namespace ConfigurationDb
{
    public class SettingsService : ISettingsService
    {
        private readonly ISettingsRepository repository;

        public SettingsService(ISettingsRepository repository)
        {
            this.repository = repository;
        }
        public void SetSettingsForTarget(string target, IEnumerable<Setting> settings)
        {
            using(this.repository)
            {
                //Removed settings
                IEnumerable<Setting> removedSettings = repository.GetAllByTarget(target).Where(s1 => !settings.Any(s2 => s2.Key == s1.Key && s2.Target == s1.Target));
                repository.Delete(removedSettings);

                //Upsert new/modifed settings
                foreach(Setting setting in settings)
                {
                    var currentSetting = repository.Find(s => string.Equals(s.Key, setting.Key) && string.Equals(s.Target, setting.Target)).FirstOrDefault();
                    if(currentSetting == null)
                    {
                        repository.Add(setting);
                    }
                    else
                    {
                        repository.Replace(currentSetting, setting);
                    }
                }
                repository.SaveChanges();
            }
        }

        public IEnumerable<Setting> GetSettingsByTarget(string target)
        {
            using (this.repository)
            {
                return repository.GetAllByTarget(target).ToList();
            }
        }

        public IEnumerable<string> GetTargets()
        {
            using (this.repository)
            {
                return repository.Find(s => s.Target != null).Select(s => s.Target).ToList().Distinct();
            }
        }

        public void DeleteTarget(string target)
        {
            using (this.repository)
            {
                repository.Delete(repository.GetAllByTarget(target));
                repository.SaveChanges();
            }
        }
    }
}
