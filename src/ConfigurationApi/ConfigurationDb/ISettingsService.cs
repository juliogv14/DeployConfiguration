﻿using ConfigurationDb.Domain;
using System.Collections.Generic;

namespace ConfigurationDb
{
    public interface ISettingsService
    {
        void SetSettingsForTarget(string target, IEnumerable<Setting> settings);
        IEnumerable<Setting> GetSettingsByTarget(string target);

        IEnumerable<string> GetTargets();

        void DeleteTarget(string target);
    }
}