﻿using ConfigurationDb.Domain;
using Microsoft.EntityFrameworkCore;

namespace ConfigurationDb
{
    public class SettingsDbContext : DbContext
    {
        public DbSet<Setting> Settings { get; set; }

        public SettingsDbContext(DbContextOptions<SettingsDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            DefineDatabase(builder);
        }

        private void DefineDatabase(ModelBuilder builder)
        {
            builder.Entity<Setting>().ToTable("Settings");
            builder.Entity<Setting>().HasKey(s => new
            {
                s.Key,
                s.Target
            });
            builder.Entity<Setting>().Property(s => s.Key).IsRequired().HasMaxLength(50);
            builder.Entity<Setting>().Property(s => s.Target).IsRequired().HasMaxLength(25);
            builder.Entity<Setting>().Property(s => s.Value).IsRequired().HasMaxLength(500);

        }
    }
}
