﻿using ConfigurationDb.Domain;
using System;
using System.Collections.Generic;

namespace ConfigurationDb
{
    public interface ISettingsRepository : IDisposable
    {
        void Add(Setting settings);

        void Replace(Setting oldsetting, Setting newSetting);

        void Delete(IEnumerable<Setting> setting);

        IEnumerable<Setting> Find(Func<Setting, bool> filter);

        IEnumerable<Setting> GetAllByTarget (string target);

        void SaveChanges();
    }
}