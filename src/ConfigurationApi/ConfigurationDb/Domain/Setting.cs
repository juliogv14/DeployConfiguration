﻿namespace ConfigurationDb.Domain
{
    public class Setting
    {
        public string Key { get; set; }

        public string Target { get; set; }

        public string Value { get; set; }
    }
}
