﻿using ConfigurationDb.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConfigurationDb
{
    public class EfSettingsRepository : ISettingsRepository
    {
        private readonly DbSet<Setting> dbSet;

        private readonly SettingsDbContext context;

        public EfSettingsRepository(SettingsDbContext context)
        {
            this.context = context;
            this.dbSet = context.Set<Setting>();
        }

        public void Add(Setting setting) => this.dbSet.Add(setting);

        public void Replace(Setting oldsetting, Setting newSetting) => this.context.Entry(oldsetting).CurrentValues.SetValues(newSetting);

        public void Delete(IEnumerable<Setting> settings) => this.dbSet.RemoveRange(settings);

        public IEnumerable<Setting> Find(Func<Setting, bool> filter) => this.dbSet.Where(filter);

        public IEnumerable<Setting> GetAllByTarget(string target) => this.dbSet.Where(s => s.Target == target);

        public void SaveChanges() => this.context.SaveChanges();

        public void Dispose()
        {
            this.context.Dispose();
        }
    }
}
